#include "EventLoop.h"
#include "KQueue.h"
#include "Epoll.h"

namespace snet
{

LoopHandlerSet::LoopHandlerSet()
{
}

void LoopHandlerSet::AddLoopHandler(LoopHandler *lh)
{
    set_.insert(lh);
}

void LoopHandlerSet::DelLoopHandler(LoopHandler *lh)
{
    set_.erase(lh);
}

void LoopHandlerSet::HandleLoop()
{
    for (auto lh : set_)
        lh->HandleLoop();
}

void LoopHandlerSet::HandleStop()
{
    for (auto lh : set_)
        lh->HandleStop();
}

std::unique_ptr<EventLoop> CreateEventLoop(unsigned int time_out_ns)
{
#ifdef __APPLE__
    return std::unique_ptr<EventLoop>(new KQueue(time_out_ns));
#elif __linux__
    return std::unique_ptr<EventLoop>(new Epoll(time_out_ns));
#else
#error "Platform is not support"
#endif
}

} // namespace snet
