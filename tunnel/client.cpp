#include "relay.h"
#include <stdio.h>
#include <stdlib.h>
#include <memory>
#include <iostream>
#include <unordered_map>


class Server final
{
public:
    Server(const std::string &tunnel_ip, unsigned short tunnel_port,
           snet::EventLoop *loop, snet::TimerList *timer_list)
        : tunnel_port_(tunnel_port),
          tunnel_ip_(tunnel_ip),
          loop_(loop),
          tunnel_reconnect_timer_(timer_list), enable_send_(false)
    {
        CreateTunnel();
    }

    Server(const Server &) = delete;
    void operator = (const Server &) = delete;

    bool IsEnableSend() {
        return enable_send_;
    }

    bool CanSend() {
        return IsEnableSend() && tunnel_->CanSend();
    }

    void Send(std::unique_ptr<snet::Buffer> data) {
        tunnel_->Send(std::move(data));
    }

private:
    void CreateTunnel()
    {
        std::cout << "[Create New Tunnel]" << std::endl;
        tunnel_.reset(new relay::Client(tunnel_ip_, tunnel_port_, loop_));
        tunnel_->SetErrorHandler(
            [this] () {
                HandleTunnelError();
            });
        tunnel_->SetDataHandler(
            [this] (std::unique_ptr<snet::Buffer> data) {
                HandleTunnelData(std::move(data));
            });
        tunnel_->Connect(
            [this] () {
                HandleTunnelConnected();
            });
    }

    void HandleTunnelError()
    {
        enable_send_ = false;
        tunnel_reconnect_timer_.ExpireFromNow(snet::Seconds(1));
        tunnel_reconnect_timer_.SetOnTimeout([this] () { CreateTunnel(); });
    }

    void HandleTunnelData(std::unique_ptr<snet::Buffer> data)
    {
        std::cerr << std::string(data->buf, data->buf + data->pos) << std::endl;
    }

    void HandleTunnelConnected()
    {
        enable_send_ = true;
    }

    unsigned short tunnel_port_;
    std::string tunnel_ip_;

    snet::EventLoop *loop_;

    snet::Timer tunnel_reconnect_timer_;
    std::unique_ptr<relay::Client> tunnel_;
    bool enable_send_;
};

int main(int argc, const char **argv)
{
    if (argc != 3)
    {
        fprintf(stderr, "Usage: %s ServerIP Port\n", argv[0]);
        return 1;
    }

    signal(SIGPIPE, SIG_IGN);

    auto event_loop = snet::CreateEventLoop();
    snet::TimerList timer_list;

    Server server(argv[1], atoi(argv[2]), event_loop.get(), &timer_list);

    snet::Timer send_timer(&timer_list);
    send_timer.ExpireFromNow(snet::Milliseconds(0));
    send_timer.SetOnTimeout(
        [&server, &send_timer] () {
            using namespace snet;
            std::unique_ptr<Buffer> data(new Buffer(new char[100],
                                         sizeof(char[100]),
                                         OpDeleter));
            std::generate(data.get()->buf, data.get()->buf + 100,
                          [] { return static_cast<char>(std::rand() % 128); });
            if (server.CanSend()) {
                server.Send(std::move(data));
            }
            send_timer.ExpireFromNow(snet::Milliseconds(0));
        });


    snet::TimerDriver timer_driver(timer_list);
    event_loop->AddLoopHandler(&timer_driver);
    event_loop->Loop();

    return 0;
}
