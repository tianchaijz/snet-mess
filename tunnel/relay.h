#ifndef RELAY_H
#define RELAY_H

#include <string>

#include "Acceptor.h"
#include "Connector.h"
#include "Connection.h"
#include "EventLoop.h"
#include "Timer.h"

namespace relay
{

class Connection final
{
public:
    using ErrorHandler = std::function<void ()>;
    using DataHandler = std::function<void (std::unique_ptr<snet::Buffer>)>;

    Connection(std::unique_ptr<snet::Connection> connection);

    Connection(const Connection &) = delete;
    void operator = (const Connection &) = delete;

    void SetErrorHandler(const ErrorHandler &error_handler);
    void SetDataHandler(const DataHandler &data_handler);
    int Send(std::unique_ptr<snet::Buffer> buffer);
    bool CanSend();

private:
    int SendBuffer(std::unique_ptr<snet::Buffer> buffer);
    void HandleError();
    void HandleReceivable();

    static const int kLengthBytes = 1024;

    char recv_length_[kLengthBytes];
    snet::Buffer recv_length_buffer_;

    ErrorHandler error_handler_;
    DataHandler data_handler_;
    std::unique_ptr<snet::Connection> connection_;
};


class Client final
{
public:
    using OnConnected = std::function<void ()>;
    using ErrorHandler = std::function<void ()>;
    using DataHandler = std::function<void (std::unique_ptr<snet::Buffer>)>;

    Client(const std::string &ip, unsigned short port, snet::EventLoop *loop);

    Client(const Client &) = delete;
    void operator = (const Client &) = delete;

    void SetErrorHandler(const ErrorHandler &error_handler);
    void SetDataHandler(const DataHandler &data_handler);
    void Connect(const OnConnected &onc);
    bool Send(std::unique_ptr<snet::Buffer> buffer);
    bool CanSend();

private:
    void HandleConnect(std::unique_ptr<snet::Connection> connection,
                       const OnConnected &onc);

    ErrorHandler error_handler_;
    DataHandler data_handler_;

    snet::Connector connector_;
    std::unique_ptr<Connection> connection_;
};

} // namespace relay

#endif // RELAY_H
