#include "relay.h"
#include <arpa/inet.h>
#include <string.h>
#include <iostream>

namespace relay
{

Connection::Connection(std::unique_ptr<snet::Connection> connection)
    : recv_length_buffer_(recv_length_, sizeof(recv_length_)),
      connection_(std::move(connection))
{
    memset(recv_length_, 0, sizeof(recv_length_));

    connection_->SetOnError([this] () { HandleError(); });
    connection_->SetOnReceivable([this] () { HandleReceivable(); });
}

void Connection::SetErrorHandler(const ErrorHandler &error_handler)
{
    error_handler_ = error_handler;
}

void Connection::SetDataHandler(const DataHandler &data_handler)
{
    data_handler_ = data_handler;
}

int Connection::Send(std::unique_ptr<snet::Buffer> buffer)
{
    return SendBuffer(std::move(buffer));
}

int Connection::SendBuffer(std::unique_ptr<snet::Buffer> buffer)
{
    if (connection_->Send(std::move(buffer)) ==
        static_cast<int>(snet::SendE::Error))
    {
        std::cerr << "ERROR" << std::endl;
        error_handler_();
        return false;
    }

    return true;
}

void Connection::HandleError()
{
    error_handler_();
}

void Connection::HandleReceivable()
{
    if (recv_length_buffer_.pos < recv_length_buffer_.size) {
        auto ret = connection_->Recv(&recv_length_buffer_);
        if (ret == static_cast<int>(snet::RecvE::PeerClosed) ||
            ret == static_cast<int>(snet::RecvE::Error))
        {
            error_handler_();
            return ;
        }

        if (ret > 0)
        {
            recv_length_buffer_.pos += ret;
        }
    }

    auto length = recv_length_buffer_.pos;
    auto data = new char[length];
    std::copy(recv_length_buffer_.buf, recv_length_buffer_.buf + length, data);
    std::unique_ptr<snet::Buffer> ptr(new snet::Buffer(data, length, snet::OpDeleter));
    recv_length_buffer_.pos = 0;
    data_handler_(std::move(ptr));
}

bool Connection::CanSend() {
    return connection_ && !connection_->SendQueueFull();
}

Client::Client(const std::string &ip, unsigned short port,
               snet::EventLoop *loop)
    : connector_(ip, port, loop)
{
}

void Client::SetErrorHandler(const ErrorHandler &error_handler)
{
    error_handler_ = error_handler;
}

void Client::SetDataHandler(const DataHandler &data_handler)
{
    data_handler_ = data_handler;
}

void Client::Connect(const OnConnected &onc)
{
    connector_.Connect(
        [this, onc] (std::unique_ptr<snet::Connection> connection) {
            HandleConnect(std::move(connection), onc);
        });
}

bool Client::Send(std::unique_ptr<snet::Buffer> buffer)
{
    return connection_->Send(std::move(buffer));
}

void Client::HandleConnect(std::unique_ptr<snet::Connection> connection,
                           const OnConnected &onc)
{
    if (connection)
    {
        connection_.reset(new Connection(std::move(connection)));
        connection_->SetErrorHandler(error_handler_);
        connection_->SetDataHandler(data_handler_);
        onc();
    }
    else
    {
        error_handler_();
    }
}

bool Client::CanSend() {
    return connection_->CanSend();
}

} // namespace tunnel
